//Write a program to find the Reverse of a string 


**Question 1: Reverse a String**
**Problem Statement:** Write a function that takes a string as input and returns the string reversed.

**Solution:**
```javascript
function reverseString(str) {
  return str.split('').reverse().join('');
}
```

**Question 2: Check Palindrome**
**Problem Statement:** Write a function that checks whether a given string is a palindrome (reads the same forwards and backwards).

**Solution:**
```javascript
function isPalindrome(str) {
  const cleanedStr = str.toLowerCase().replace(/[^a-z0-9]/g, '');
  const reversedStr = cleanedStr.split('').reverse().join('');
  return cleanedStr === reversedStr;
}
```

**Question 3: Find the Longest Word**
**Problem Statement:** Write a function that takes a sentence as input and returns the longest word in the sentence.

**Solution:**
```javascript
function findLongestWord(sentence) {
  const words = sentence.split(' ');
  let longestWord = '';
  for (const word of words) {
    if (word.length > longestWord.length) {
      longestWord = word;
    }
  }
  return longestWord;
}
```

**Question 4: Count Vowels**
**Problem Statement:** Write a function that counts the number of vowels in a given string.

**Solution:**
```javascript
function countVowels(str) {
  const vowels = 'aeiouAEIOU';
  let count = 0;
  for (const char of str) {
    if (vowels.includes(char)) {
      count++;
    }
  }
  return count;
}
```

**Question 5: Fibonacci Series**
**Problem Statement:** Write a function that generates the Fibonacci series up to a given number of terms.

**Solution:**
```javascript
function fibonacciSeries(n) {
  const series = [0, 1];
  for (let i = 2; i < n; i++) {
    series[i] = series[i - 1] + series[i - 2];
  }
  return series;
}
```

**Question 6: Capitalize Words**
**Problem Statement:** Write a function that capitalizes the first letter of each word in a given sentence.

**Solution:**
```javascript
function capitalizeWords(sentence) {
  return sentence.replace(/\b\w/g, char => char.toUpperCase());
}
```

**Question 7: Find Max Number**
**Problem Statement:** Write a function that finds the maximum number in a given array of numbers.

**Solution:**
```javascript
function findMaxNumber(numbers) {
  return Math.max(...numbers);
}
```

**Question 8: Array Sum**
**Problem Statement:** Write a function that calculates the sum of all numbers in a given array.

**Solution:**
```javascript
function arraySum(numbers) {
  return numbers.reduce((sum, num) => sum + num, 0);
}
```

**Question 9: Remove Duplicates**
**Problem Statement:** Write a function that removes duplicate values from an array.

**Solution:**
```javascript
function removeDuplicates(arr) {
  return [...new Set(arr)];
}
```

**Question 10: Check Prime Number**
**Problem Statement:** Write a function that checks whether a given number is prime.

**Solution:**
```javascript
function isPrime(number) {
  if (number <= 1) {
    return false;
  }
  for (let i = 2; i <= Math.sqrt(number); i++) {
    if (number % i === 0) {
      return false;
    }
  }
  return true;
}
```




//